require("dotenv").config();
const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");

//import  routes
const patientRoutes = require("./routes/patient");
const insuranceRoutes = require("./routes/insurance");
const handleErrors = require("./utils/handleErrors");

//DB connection
mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("DB CONNECTED");
  });

app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());

//Use Routes
app.use("/api", patientRoutes);
app.use("/api", insuranceRoutes);

//An error handling middleware
// app.use(function(err, req, res, next) {
//   console.log("express error handler called");
//   res.status(500).json({
//     error:err
//   })
// });

app.use(handleErrors);


//PORT
const port = process.env.PORT || 8000;
// app.get("/",(req, res)=>{
//     console.log("========")
//     res.send("Backend hit")
// })
//Starting server
app.listen(port, () => {
  console.log(`app is running at ${port}`);
});