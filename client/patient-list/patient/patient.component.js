
var app=angular.module('patientDetail',[]);
app.component('patientDetail',{
    templateUrl: './patient-list/patient/patient.template.html',
    controller:['$routeParams','$http','$scope','$location',function PatientController($routeParams,$http,$scope,$location){

        $http.get("http://localhost:8000/api/patient/"+ $routeParams.patientId)
        .then((res)=>{
            $scope.patient=res.data;
            $scope.insurance=res.data.insurance
        },
        (err)=>{
            $scope.error=err;
        }
        )
        },
    ]     
    });

app.controller('editPatientCtrl',function($scope,$http,$routeParams,$location){

    $http.get("http://localhost:8000/api/patient/"+ $routeParams.patientId)
        .then((res)=>{
            $scope.patient=res.data;
            $scope.insurance=res.data.insurance
            //console.log($scope.insurance);
        },
        (err)=>{
            $scope.error=err;
        }
        )

        const redirect=()=>{
            $location.path("/patient/"+$scope.patient._id)
            $scope.$apply()
            //$location.reload();
        }
   
        //CREATE FORM SHOULD BE VISIBILE..??
        $scope.showForm=true;
        $scope.updateForm=function(){
            $scope.showForm=!$scope.showForm;
        }
        
        $http.get("http://localhost:8000/api/insurances")
            .then((res)=>{
                $scope.insurances=res.data;
            }).catch((err)=>{
                $scope.error=err;
            });

        $scope.master = {};

        $scope.update = function(patient) {

            $scope.master = angular.copy(patient);
            $http({
                    method: 'PUT',
                    url: 'http://localhost:8000/api/patient/'+$scope.patient._id,
                    headers: {
                    'Content-Type': 'application/json'
                    },
                    data: $scope.master
                })
                .then((res)=>{
                    $scope.updateForm();
                    swal({
                        title: "Updated",
                        text: "Patient Name  :"+patient.name,
                        icon: "success",
                        button: "Ok",
                      })
                      .then((value)=>{
                          value && redirect()
                      })                   
                })
                .catch(({data})=>{
                    swal({
                        title: "Patient not Updated!",
                        text: data.message,
                        icon: "error",
                        button: "Ok",
                      });
                });
            };  

})
