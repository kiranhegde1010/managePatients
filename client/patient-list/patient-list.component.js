var app=angular.module('patientList',[]);
app.component('patientList',{
    templateUrl: './patient-list/patient-list.template.html',
    controller:function PatientListController($scope,$http,$location){
        //GET ALL PATIENTS
         $scope.loadPatients=()=>{
            $http.get("http://localhost:8000/api/patients")
            .then((res)=>{
                $scope.patients=res.data;
                $scope.$digest();
            }).catch((err)=>{
                $scope.error=err;
            });
        }
        $scope.loadPatients();
        
        //DELETE PATIENT
        $scope.deletePatient= function(patientId){

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this later",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    $http({
                        method: 'DELETE',
                        url: 'http://localhost:8000/api/patient/'+patientId,
                    }).then(
                        swal("Patient has been deleted!", {
                            icon: "success",
                          })
                          .then((value)=>{
                              value && $scope.loadPatients();
                          })
                    ) 
                } else {
                  swal("not deleted")
                }
              })
              .catch(err=>console.log(err)) 
        }
        }
    });

//CREATE PATIENT
app.controller("createPatientCtrl",function($scope,$http,$location){
        $scope.showForm=true;
        //CREATE FORM SHOULD BE VISIBILE..??
        $scope.createForm=function(){
            $scope.showForm=!$scope.showForm;
        }
        const redirect=()=>{
            $location.path("/")
            $scope.$apply()
        }
        //FORM CONTROLLER
        $http.get("http://localhost:8000/api/insurances")
            .then((res)=>{
                $scope.insurances=res.data;
            }).catch((err)=>{
                $scope.error=err;
            });

        $scope.master = {};

        $scope.createNew = function(newPatient) {
            
            $scope.master = angular.copy(newPatient);
            $http({
                    method: 'POST',
                    url: 'http://localhost:8000/api/patient/create',
                    headers: {
                    'Content-Type': 'application/json'
                    },
                    data: $scope.master
                })
                .then(()=>{
                   $scope.createForm();
                   swal({
                    title: "Created",
                    text: "New Patient  :"+newPatient.name,
                    icon: "success",
                    button: "Ok",
                  })
                  .then((value)=>{
                      value && redirect()
                  })
                   
                })
                .catch(({data})=>{
                    swal({
                        title: "Patient not created!",
                        text: data.message,
                        icon: "error",
                        button: "Ok",
                      });
                });
            };  
})
