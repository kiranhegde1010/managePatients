
var app=angular.module("myApp",['patientList','patientDetail','ngRoute'])

app.config(['$routeProvider', '$locationProvider',
function config($routeProvider,$locationProvider) {
    $locationProvider.hashPrefix('');
  $routeProvider.
    when('/', {
      template: '<patient-list></patient-list>'
    }).
    when('/patient/create', {
      templateUrl:'./patient-list/patient/create.patient.html',
      controller:'createPatientCtrl'
    }).
    when('/patient/:patientId', {
      template:'<patient-detail></patient-detail>',
    }).
    when('/patient/edit/:patientId', {
      templateUrl:'./patient-list/patient/edit.patient.html',
      controller:'editPatientCtrl'
    }).
    otherwise('/');
 }
]);

 
