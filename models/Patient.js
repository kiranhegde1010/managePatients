const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const patientSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      maxlength: 32,
    },
    age: {
      type: Number,
      required: true,
      maxlength: 150,
      trim: true,
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true,
      },
    phone: {
        type: Number,
        required: true,
        trim: true,
      },
    insurance: {
      type: ObjectId,
      ref: "Insurance",
     // required: true,
    },
 
    // photo: {
    //   data: Buffer,
    //   contentType: String,
    // },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Patient", patientSchema);