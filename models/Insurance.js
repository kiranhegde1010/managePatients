const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const insuranceSchema = new mongoose.Schema(
  {
    name:{
        type:String,
        required:true,
        trim:true
    },
    amount:{
        type:Number,
        required:true,
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Insurance", insuranceSchema);