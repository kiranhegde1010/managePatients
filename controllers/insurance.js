const Insurance = require("../models/Insurance");
const { check, validationResult } = require("express-validator");

exports.getInsuranceById = async (req, res, next, id) => {
    try{
      let insurance=await Insurance.findById(id).exec();
      if(insurance==null){
        return res.status(404).json({
          error:"patient not found in DB"
        })
      } 
      req.insurance = insurance;
    }catch{
      return res.status(400).json({
        error: "patient not found in DB",
      });
    }

    next();  
  };

//CREATE
exports.createInsurance=async (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
        error: errors.array()[0].msg,
        });
    }

    let insurance = new Insurance(req.body);
    try{
      await insurance.save();
      res.json({
        name: insurance.name,
        email: insurance.amount,
        id: insurance._id,
        });
    }catch{
      return res.status(400).json({
        err: "NOT able to save the Inurance  in DB",
      });
    }
}

//READ
exports.getInsurance=(req,res)=>{
  return res.json(req.insurance);
}

//LISTING
exports.getAllInsurances=async (req,res)=>{
  try{
    let insurances=await Insurance.find().exec();
    return res.json(insurances);
  }catch{
    return res.status(400).json({
      error:"no insurances found in DB"
    })
  }
}

//UPDATE
exports.updateInsurance =async (req, res) => {
  let insurance = req.body
  const filter={_id: req.insurance._id}
  
  req.insurance.modified = new Date();
  try{
    await Insurance.findOneAndUpdate(filter,insurance)
    console.log("updated")
    return res.json(insurance);
  }catch{
    return res.status(400).json({
      error: "NOT able to update the Insurance in DB"
    });
  }
};

//DELETE
exports.deleteInsurance =async (req, res) => {
    let insurance = req.insurance;
    try{
      let deletedInsurance=await patient.remove()
      return res.json({
        message: "Deletion was success",
        deletedInsurance,
      });
    }catch{
      return res.status(400).json({
        error: "Failed to delete the insurance",
      });
    }
  };

//SEARCH
exports.searchInsurances=async (req,res)=>{
  const pattern=req.query.name
  const limit=1
  const skip=limit*(req.query.page-1)
  try{
    let insurances=await Insurance.find({ "name": {$regex:`${pattern}`,$options:'i'}}).skip(skip).limit(limit)
    return res.json(insurances);
  }catch{
    return res.status(400).json({
      error:"No records found for search"
    })
  }
}
