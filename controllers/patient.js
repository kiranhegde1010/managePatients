const Patient = require("../models/Patient");
const { check, validationResult } = require("express-validator");
const { BadRequest, NotFound } = require("../utils/errors");

exports.getPatienById = async (req, res, next, id) => {
  let patient;
  try{
    patient= await Patient.findById(id).populate("insurance").exec();
    if(patient==null){
      throw new NotFound("patient not found in DB")
    }
  }catch(err){
    next(err)
  }
  req.patient = patient;
  next();  
};

//CREATE
exports.createPatient=async (req,res,next)=>{
    let errors = validationResult(req);
    try{
      if (!errors.isEmpty()) {
          throw new BadRequest(errors.array()[0].msg);
      }
      const patient = new Patient(req.body);

      let newPatient = await patient.save();
      return res.status(201).json({
        name: newPatient.name,
        email: newPatient.email,
        id: newPatient._id,
        })
    }catch(err){
    next(err)
    }
}

//READ
exports.getPatient=(req,res)=>{
  return res.json(req.patient);
}
//LISTING
exports.getAllPatients =async (req,res,next)=>{
//let sortBy = req.query.sortBy ? req.query.sortBy : "_id";
  let patients;
  try{
    patients= await Patient.find().populate("insurance")
              //.sort([[sortBy, "asc"]])
              .exec();
    return res.json(patients)
  }catch(err){
    next(err)
  }

}

//UPDATE
exports.updatePatient = async (req, res,next) => {
  try{
    const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw new BadRequest(errors.array()[0].msg);
      }
    let patient = req.body
    const filter={_id: req.patient._id}

    req.patient.modified = new Date();
    
    await Patient.findOneAndUpdate(filter,patient);
    return res.json(patient)
  }catch(err){
    next(err);
  }

};

//DELETE
exports.deletePatient = async (req, res) => {
  let patient = req.patient;
  try{
    let deletedPatient=await patient.remove();
    return res.json({
      message: "Deletion was success",
      deletedPatient,
    });
  }catch(err){
   next(err)
  }
};

//SEARCH
exports.searchPatients=async (req,res)=>{
  try{
    const pattern=req.query.name
    const limit= 2  //req.query.limit
    //console.log(limit)
    const skip=limit*(req.query.page-1)
  
    let patients= await Patient.find({ "name": {$regex:`${pattern}`,$options:'i'}}).skip(skip).limit(limit);
    return  res.json(patients);
  }catch(err){
   next(err)
  }
}