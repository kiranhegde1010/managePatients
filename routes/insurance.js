const express=require("express");
const router =express.Router();
const { check, validationResult } = require("express-validator");
const { createInsurance, getAllInsurances, getInsurance,getInsuranceById,deleteInsurance, updateInsurance, searchInsurances } = require("../controllers/insurance");

//PARAMS
router.param("insuranceId",getInsuranceById);


//ROUTES
//create
router.post(
    "/insurance/create/",
    [
        check("name", "name should be atleast 4 char").isLength({ min: 4 }),
        check("amount", "Age Shuould be number").isNumeric(),
    ],
    createInsurance
);

//read
router.get(
    "/insurances",
    getAllInsurances
)

router.get(
    "/insurance/:insuranceId",
    getInsurance
)

//update
router.put(
    "/insurance/:insuranceId",
    updateInsurance
)

//delete
router.delete(
    "/insurance/:insuranceId",
    deleteInsurance
)
//search
router.get(
    "/insurances/search",
    searchInsurances
)

module.exports = router;