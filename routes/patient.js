const express=require("express");
const router =express.Router();
const { check, validationResult } = require("express-validator");
const { createPatient, getAllPatients, getPatienById, deletePatient,updatePatient, searchPatients, getPatient } = require("../controllers/patient");

//PARAMS
router.param("patientId",getPatienById);

//ROUTES
//create
router.post(
    "/patient/create/",
    [
        check("name", "name should be atleast 4 char").isLength({ min: 4 }),
        check("age", "Age Shuould be number").isNumeric(),
        check("email", "email format is not proper").isEmail(),
        check("phone", "phone number should be 10 digit").isLength({min:10,max:10})
    ],
    createPatient
);

//read
router.get(
    "/patients",
    getAllPatients
)

router.get(
    "/patient/:patientId",
    getPatient
)

//update
router.put(
    "/patient/:patientId",
    [
        check("name", "name should be atleast 4 char").isLength({ min: 4 }),
        check("age", "Age Shuould be number").isNumeric(),
        check("email", "email format is not proper").isEmail(),
        check("phone", "phone number should be 10 digit").isLength({min:10,max:10})
    ],
    updatePatient
)

//delete
router.delete(
    "/patient/:patientId",
    deletePatient
)

//search
router.get(
    "/patients/search/",
    searchPatients
)
module.exports = router;